module.exports = {
  assetsDir: '',
  publicPath: process.env.NODE_ENV === 'production'
  ? './'
  : '/',
  css: {
    loaderOptions: {
      scss: {
          prependData: `@import "@/assets/scss/colors.scss";`
        },
    }
  }
}