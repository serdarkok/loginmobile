import axios from 'axios';
import qs from 'querystring';

const apiurl = process.env.VUE_APP_POST_LOGIN_URL;
let response;
async function get(endpoint, data, header) {
  try {
    response = await axios({
      method: 'get',
      baseURL: apiurl,
      url: endpoint,
      timeout: 3000,
      params: {
        ...data,
      },
      withCredentials: true,
      credentials: 'same-origin',
      headers: {
        Authorization: `Bearer ${header}`,
        'content-Type': 'application/json',
      },
    });
    return {
      success: true,
      data: response.data,
    };
  } catch (error) {
    return {
      success: false,
      error: 'An error has occurred while establishing a connect to the server.',
    };
  }
}

async function post(endpoint, data) {
  try {
    response = await axios({
      method: 'post',
      baseURL: apiurl,
      url: endpoint,
      timeout: 3000,
      headers: {
        'content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify(data),
    });
    return {
      success: true,
      message: response.data,
    };
  } catch (error) {
    return {
      success: false,
      error: error.response.data,
    };
  }
}

export {
  post,
  get,
};
