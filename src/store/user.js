import { get } from '@/api';
import router from '../router/index';

const actions = {
  async getToken({ commit }, data) {
    await commit('ADD_TOKEN', data);
    let userInformation;
    try {
      // get(url, params, headers)
      userInformation = await get('/core/me', null, data.access_token);
      if (!userInformation.success) {
        userInformation = {
          data: {
            id: 6809163,
            name: 'Serdar Kök',
            complaintId: 13767534,
            picture: '/pp/mbr/3e/98/3e98d4915a607431ab4cda79ee886dd7.jpg?1595794886',
            phone: '+905334681406',
            phoneVerificationTime: '2020-03-04 16:52:12.000+0300',
            email: 'kokserdal@gmail.com',
            emailVerificationTime: null,
            registrationTime: '2020-03-04 16:51:52.000+0300',
            deleteTime: null,
            passwordExists: true,
          },
        };
      }
    } catch (error) {
      // eslint-disable-next-line
      console.log(error);
    } finally {
      localStorage.setItem('user_information', JSON.stringify(userInformation.data));
      await commit('ADD_USER_INFORMATION', userInformation.data);
      router.push('/personal');
    }
  },
};

const mutations = {
  ADD_TOKEN(state, data) {
    state.token = data.access_token;
    localStorage.setItem('access_token', state.token);
  },

  ADD_USER_INFORMATION(state, data) {
    state.user = data;
  },
};

const getters = {
  getUserInformation(state) {
    return state.user;
  },
};

const state = () => ({
  token: null,
  user: {
    id: '',
    name: '',
    complaintId: '',
    picture: '',
    phone: '',
    phoneVerificationTime: '',
    email: '',
    emailVerificationTime: '',
    registrationTime: '',
    deleteTime: '',
    passwordExists: '',
  },
});

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
