import Vue from 'vue';
import Vuex from 'vuex';
import User from './user';

Vue.use(Vuex);

const createStore = () => new Vuex.Store({
  modules: {
    user: User,
  },
});

export default createStore;
