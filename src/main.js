import Vue from 'vue';
import dotenv from 'dotenv';
import { BContainer, BRow, BCol } from 'bootstrap-vue';
import VueSweetalert2 from 'vue-sweetalert2';
import App from './App.vue';
import router from './router';
import store from './store';
import 'sweetalert2/dist/sweetalert2.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

const SweetAlertOptions = {
  confirmButtonColor: '#45da6f',
  position: 'top',
};

Vue.config.productionTip = false;
Vue.use(VueSweetalert2, SweetAlertOptions);
Vue.component('b-container', BContainer);
Vue.component('b-row', BRow);
Vue.component('b-col', BCol);
dotenv.config();
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
